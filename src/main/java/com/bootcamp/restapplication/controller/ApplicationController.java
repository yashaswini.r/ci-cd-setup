package com.bootcamp.restapplication.controller;

import com.bootcamp.restapplication.dto.ApplicationDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

    @GetMapping("/application")
    public ApplicationDTO getApplicationName()
    {
        ApplicationDTO applicationDTO = new ApplicationDTO();
        applicationDTO.setName("Yashaswini&Ashish");
        return applicationDTO;
    }
}

