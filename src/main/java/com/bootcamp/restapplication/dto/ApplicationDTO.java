package com.bootcamp.restapplication.dto;

public class ApplicationDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
