package com.bootcamp.restapplication.integrations;

import com.bootcamp.restapplication.dto.ApplicationDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationControllerIntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void should_return_name_when_get_application_endpoint_is_hit(){

        ResponseEntity<ApplicationDTO> response = testRestTemplate.exchange("/application", HttpMethod.GET, null, ApplicationDTO.class);
        ApplicationDTO dto = response.getBody();
        assertEquals("Yashaswini&Ashish", dto.getName());
    }
}
