package com.bootcamp.restapplication.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ApplicationControllerTest {

    @InjectMocks
    ApplicationController controller;

    @Test
    public void should_return_application_name(){
        assertEquals("Yashaswini&Ashish", controller.getApplicationName().getName());
    }
}
